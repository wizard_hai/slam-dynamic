// 该文件将打开你电脑的摄像头，并将图像传递给ORB-SLAM2进行定位
 
// opencv
#include <opencv2/opencv.hpp>
 
// ORB-SLAM的系统接口
#include "System.h"
#include <string>
#include <chrono>
#include <iostream>

using namespace std;

string parameterFile = "./myslam.yaml";
string vocFile = "../Vocabulary/ORBvoc.txt";

int main()
{
    ORB_SLAM2::System SLAM(vocFile, parameterFile, ORB_SLAM2::System::MONOCULAR, true);
    
    cv::VideoCapture cap(0);
    // cv::VideoCapture cap;
    // cap.open("http://admin:admin@192.168.1.5:8081/"); //mobile camera
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

    auto start = chrono::system_clock::now();

    cv::Mat frame;
    while (cap.read(frame))
    {
        auto now = chrono::system_clock::now();
        auto timestamp = chrono::duration_cast<chrono::milliseconds>(now - start);
        SLAM.TrackMonocular(frame, double(timestamp.count())/1000.0);
    }
    // Stop all threads
    SLAM.Shutdown();
    
}